---
layout: page
title: livespectra
active: livespectra
permalink: /livespectra/
---

Livespectra est un logiciel libre en GPL V3 qui permet d'extraire un spectre et de le calibrer en direct à partir d'un film ou d'une caméra vidéo sur laquelle on a branché un dispositif permettant de faire des spectres

### Téléchargement

### Installation

### Captures d'écran

### Licence

**livespectra** est un logiciel distribué sous licence [GPLv3](https://www.gnu.org/licenses/gpl-3.0.txt).
