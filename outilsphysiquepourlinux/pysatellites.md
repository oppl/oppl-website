---
layout: page
title: pysatellites
active: pysatellites
permalink: /pysatellites/
---

Pysatellites est un logiciel GPL3 écrit en pyQT4 qui permet la visualisation d'un orbite satellitaire autour d'une masse donnée et d'en faire varier les différents paramètres.

### Téléchargement

### Installation

### Captures d'écran

### Licence

**pysatellites** est un logiciel distribué sous licence [GPLv3](https://www.gnu.org/licenses/gpl-3.0.txt).
