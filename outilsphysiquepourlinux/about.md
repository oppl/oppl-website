---
layout: page
title: À propos
active: about
permalink: /a-propos/
---

### Licence

Ce site web est sous licence [GPLv3](http://www.gnu.org/licenses/gpl-3.0.txt). Vous pouvez retrouvre son code sur [TODO](#).

Les outils suivants ont permis la réalisation de ce site :

* [Jekyll](http://jekyllrb.com/) sous licence [MIT](https://github.com/jekyll/jekyll/blob/master/LICENSE) ;
* [Bootstrap 3](http://getbootstrap.com/) sous licence [MIT](https://github.com/twbs/bootstrap/blob/master/LICENSE) ;
* Le thème [Flatly](http://bootswatch.com/flatly/) sous licence [MIT](https://github.com/thomaspark/bootswatch/blob/gh-pages/LICENSE).

### Hébergeur

Ce site web est 
