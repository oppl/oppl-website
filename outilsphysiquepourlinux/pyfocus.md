---
layout: page
title: pyfocus
active: pyfocus
permalink: /pyfocus/
---

pyfocus aide l'utilisateur d'un appareil photo à mettre au point. Ceci peut être utilisé en astronomie par exemple. Il est fondé sur gphoto2 et distribué en GPLv3.

### Téléchargement

### Installation

### Captures d'écran

### Licence

**pyfocus** est un logiciel distribué sous licence [GPLv3](https://www.gnu.org/licenses/gpl-3.0.txt) et est fondé sur [gPhoto2](http://www.gphoto.org/proj/gphoto2/).
