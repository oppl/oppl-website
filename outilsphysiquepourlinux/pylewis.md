---
layout: page
title: pylewis
active: pylewis
permalink: /pylewis/
---

pylewis permet de créer des formules chimiques à partir d'une description rapide, ou d'une formule "dictées".

### Téléchargement

### Installation

### Captures d'écran

### Licence

**pylewis** est un logiciel distribué sous licence [GPLv3](https://www.gnu.org/licenses/gpl-3.0.txt).
