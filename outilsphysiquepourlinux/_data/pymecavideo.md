**pymecavideo**

pymecavideo est un logiciel écrit en python/QT4 qui permet d'ouvrir un grand type de vidéos
et de pointer des points particuliers de cette vidéo.

Ceci permet de faire de suivis de phénomènes physiques, de faire des suivis de mouvements, 
de calculer des vitesses instantanées, de trajectoires, de déformations.

Si on choisit plusieurs points d'intérêt, pymecavideo permet de recréer une video dans 
un référentiel lié à un autre point. Exemple : sur une vidéo avec un cycliste qui lache une balle,
on peut récréer le mouvement de la balle dans le référentiel du vélo.

Pymecavideo permet d'exporter les résultats sous diverses formes : 
--vers openoffice calc
--vers qtiplot
--vers un fichier CSV

Il ouvre tous les types de vidéos supportés par le bibliothèque opencv.

