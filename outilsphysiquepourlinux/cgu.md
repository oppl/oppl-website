---
layout: page
title: Conditions Générales d'Utilisation
active: cgu
domain: outilsphysiques.tuxfamily.org
editor: Jean-Baptiste Butet
hosting: TuxFamily.org
permalink: /cgu/
---

### 1. Objet

Les présentes « Conditions Générales d'Utilisation » (CGU) ont pour objet de définir l'encadrement juridique des modalités selon lesquelles ce site web est mis à la disposition des utilisateurs.

Toute connexion au site **{{ page.domain }}** est soumise au respect des présentes conditions générales d'utilisation. L'accès au site par l'utilisateur entraîne son acceptation des présentes conditions générales d'utilisation et constitue donc un contrat entre l'éditeur et l'utilisateur.

En cas de non-acceptation des conditions générales d'utilisation stipulées dans le présent contrat, l'utilisateur se doit de renoncer à l'accès des services proposés par le site.

### 2. Mentions légales

L'édition du site **{{ page.domain }}** est assurée par {{ page.editor }}.

Le site est hébergé par {{ page.hosting }}.

### 3. Accès au site

Le site est accessible gratuitement à tout utilisateur ayant un accès à Internet. Tous les frais supportés par l'utilisateur pour accéder au service (matériel informatique, logiciels, abonnement Internet, etc.) sont à sa charge.

L'éditeur met en œuvre tous les moyens mis à sa disposition pour assurer un accès de qualité à ses services. L'obligation étant de moyens, le site ne s'engage pas à atteindre ce résultat.

L'accès au site peut à tout moment faire l'objet d'une interruption à des fins de maintenance, de mise à jour et pour toute autre raison (raisons techniques, force majeure, etc.) sans que l'interruption n'ouvre droit à aucune obligation ni indemnisation.

Tout événement dû à un cas de force majeur ayant pour conséquence un dysfonctionnement du réseau ou du serveur n'engage pas la responsabilité de l'éditeur.

L'éditeur se réserve le droit de refuser l'accès aux services du site, unilatéralement et sans notification préalable, à tout utilisateur ne respectant pas les présentes conditions d'utilisation.


### 4. Limitation de responsabilité

L'utilisateur du site assume l'ensemble des risques découlant de l'utilisation des informations présentes sur le site ou dans les sites joignables au travers des liens hypertextes.

Le site ne saurait être responsable d'un quelconque dommage résultant d'un manquement de l'utilisateur sur ce qui précède.

Une garantie optimale de la sécurité et de la confidentialité des données transmises n'est pas assurée par le site. Toutefois, le site s'engage à mettre en œuvre tous les moyens nécessaires afin de garantir au mieux la sécurité et la confidentialité des données.

La responsabilité du site ne peut être engagée en cas de force majeure ou du fait imprévisible et insurmontable d'un tiers.

### 5. Liens hypertextes

De nombreux liens hypertextes sortants sont présents sur le site, cependant les pages web où mènent ces liens n'engagent en rien la responsabilité du site qui n'a pas le contrôle de ces liens.

### 6. Évolution des présentes conditions

L'éditeur se réserve le droit de modifier unilatéralement et à tout moment le contenu des présentes conditions générales d'utilisation.

### 7. Durée

La durée des présentes conditions générales d'utilisation est indéterminée.

### 8. Droit applicable et juridiction compétente

La législation française s'applique au présent contrat.

### 9. Licence

Ces mentions légales sont grandement inspirée par celle du site [zestedesavoir.com](http://zestedesavoir.com/pages/cgu/).
