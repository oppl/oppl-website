---
layout: page
title: pymecavideo
active: pymecavideo
permalink: /pymecavideo/
---


**Pymecavideo** est un logiciel de pointage de vidéo écrit en Python 2 permettant une analyse qualitative (changement de référentiel) et quantitative (export des pointages) réalisé en [Qt4](http://qt-project.org/) avec [FFmpeg](https://www.ffmpeg.org/). Il permet, à partir d'une vidéo de déterminer les coordonnées des points que l'on peut pointer. Un export des données est possible au format [CSV](http://fr.wikipedia.org/wiki/Comma-separated_values).

### Téléchargement et installation

#### Linux - Ubuntu

Un paquet existe pour la distribution Ubuntu. Il suffit de l'installer avec `apt-get` :

```
apt-get install python-mecavideo
```

Petit récapitulatif de la version préente dans le paquet :

{:.table.table-striped.table-bordered}
| Nom et numéro de la verrsion d'Ubuntu | Version de Pymecavideo |
| ------------------------------------- | ---------------------- |
| 12.04LTS (universe)                   | 5.5                    |
| 14.04LTS (universe)                   | 6.1                    |
| 14.10 (utopic)                        | 6.1                    |
| 15.04 (vivid)                         | 6.1                    |

### Captures d'écran

### Licence

**pymecavideo** est un logiciel distribué sous licence [GPLv3](https://www.gnu.org/licenses/gpl-3.0.txt).

### Mailing Lists

TODO
